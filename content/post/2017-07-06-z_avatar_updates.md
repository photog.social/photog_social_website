---
layout: post
title: New Avatars
date: 2017-07-06
notoc: true
tags: [announce, announcement]
---

If you hadn't noticed, today we revamped the avatars used by official photog.social accounts/bots. They should be a bit cleaner, simpler and far more appropriate for coveying the account's purpose.

## Ambassador
![ambassador.png](/img/ambassador.png)

##  Announce
![announce.png](/img/announce.png)

## Curator (Coming Soon)
![curator.png](/img/curator.png)
