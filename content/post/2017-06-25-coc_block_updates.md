---
layout: post
title: Updates (CoC, Blocked Instances)
date: 2017-06-25
notoc: true
tags: [announce, announcement]
---

Nothing too special today, just some house keeping.

Today the CoC was published to the blog [here (link)](https://blog.photog.social/coc) and the list of blocked instances updated. The instance about/more page was also updated to reflect the expanded CoC.

Notably mstdn.jp was silenced and media refused due to <em>loli</em> being legal in Japan and not where the instance is hosted.

Happy Tooting!
