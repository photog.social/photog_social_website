---
layout: post
title: social.targaryen.house
date: 2017-07-12
notoc: true
tags: [announce, announcement]
---
# social.targaryen.house

The admin of social.targaryen.house reached out to the admin team today and asked us to re-consider our instance block. After internal discussion and reviewing the e-mail we received, we have opted to lift the suspension.

# Apologies

We have also removed _nazis_ and _hostile admin_ language from the original suspension description. We used language from some other block lists for social.targaryen.house. After re-reviewing the discourse surrounding the _nazi's_ language, we'd like to apologize to Wonderfall and the other admins of social.targaryen.house. We will work to do better going forward with verbiage and rationale.

# Transparency

In the interest of transparency, the original e-mail as well as the team's response has been included below. Wonderfall posted the original e-mail [here (link)](https://social.targaryen.house/users/wonderfall/updates/64821) and we'd like to continue an open dialog (even if it's screen shots of e-mail messages).

### 2017-07-12 - Initial E-mail From Wonderfall

![transparency/2017-07-12-1-wonderfall.png](/img/transparency/2017-07-12-1-wonderfall.png)

### 2017-07-12 - photog.social Admin Response to Wonderfall

![transparency/2017-07-12-2-admin-response.png](/img/transparency/2017-07-12-2-admin-response.png)
