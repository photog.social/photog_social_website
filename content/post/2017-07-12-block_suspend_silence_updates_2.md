---
layout: post
title: Silence/Suspension Updates
date: 2017-07-12
notoc: true
tags: [announce, announcement]
---
# General Updates

Today the admin team discovered that awoo.space no longer requires suspension of instances as part of the application approval process. In the interest of federation we've removed a number of suspensions and set them to silences. We retained media blocks for the time being. If any of our users feel that media blocks should be removed or the suspension should be re-enacted, let us know. [@Ambassador](https://photog.social/@Ambassador) is all ears 😉.

The full list of updated instances is:

- social.targaryen.house [more detail here (link)](/2017-07-12-block_suspend_silence_updates)
- freezepeach.xyz
- ika.moe
- mastodon.starrevolution.org
- rainbowdash.net
- sealion.club
- shitposter.club
- social.heldscal.la
- unsafe.space
