---
layout: post
title: Website Updates
date: 2017-08-05
notoc: true
tags: [announce, announcement]
---

Quick update for everyone.

We just finished updating the way we deploy new posts to the website. There was a bit more down time than we would have liked but things should be working 100% now!

Sorry for any inconvenience.
