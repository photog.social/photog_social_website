---
title: Photo Galleries
subtitle: YOUR photos, gallery style
date: 2018-03-29
tags: [infrastructure,tech,announce,announcement,upgrades]
---

Today the Admin team is excited to announce the photog.social galleries!!!

# Artodon Gallery

We have deployed [Artodon (link)](https://gitlab.com/ElenQ/artodon). Artodon is a javascript based gallery that showcases tagged toots. We'd like to call attention to [@ekaitz_zarraga@mastodon.social](https://mastodon.social/@ekaitz_zarraga) who developed the software and worked with us to get everything working.

The gallery can be found [here (linky)](https://gallery.photog.social/index.html). We've also linked to it from the header on the blog.

The gallery will show images from toots tagged [#photography](https://photog.social/tags/photography), [#photo](https://photog.social/tags/photo) and [#photographie](https://photog.social/tags/photographie) by our local users.

# Foto Fails

We have also deployed a gallery showcasing "Foto Fails". The gallery can be found [here (linky)](https://fotofails.photog.social/index.html). We've also linked to it from the header on the blog.

The gallery will show images from toots tagged [#fotofails](https://photog.social/tags/fotofails), [#photofails](https://photog.social/tags/photofails), [#fotofail](https://photog.social/tags/fotofail) and [#photofail](https://photog.social/tags/photofail) by our local users.

# Thank you to everyone

We'd like to thank everyone for the feedback and hope you enjoy the addition to the photog.social setup.

# Deployment

If you're interested in deploying Artodon on yoru own instance we made the changes in [this branch (linky)](https://gitlab.com/photog.social/artodon/tree/gallery.photog.social).
