---
title: Infrastructure
date: 2018-03-18
tags: [infrastructure,tech,safety,announce, announcement]
---

# Let's Take  Minute

With the recent issues a big Mastodon host suffered we'd like to take a minute to break down what we are doing to ensure photog.social is safe, guarded against total failure and what some of our behind the scenes stuff looks like.

We did *NOT* suffer any problems, failures or similar. A *DIFFERENT* Mastodon host failed and lost the entire database of toots, users, etc.

# Infrastructure

The photog.social instance is hosted by [masto.host (link)](http://masto.host/). We currently pay for the 'NICHE' instance size.

[Hugo (link)](https://masto.pt/@hugo) is the brains behind masto.host and we work with him whenever something pops up.

We chose masto.host because we wanted an expert in the field who doesn't mess around and provides quality service. Hugo and masto.host have been stellar on this front.

We *could* self-host an instance but we've chosen to work with an expert rather than try to stay on top of everything necessary ourselves. Hugo has been a visible member of the Mastodon community, has helped other admins and hosts many instances.

We aren't experts and will continue to work with the real experts going forward. If that ever changes, our users will be the first to know.

The main masto.host website has good details on what they provide. A few highlights:

* OVH data centers
* Offical Mastodon code base
* Regular backups

# Backups

masto.host has internal backups (we don't have access to them) that they use to safeguard the instances they host. The details aren't posted to their main website but we did talk with Hugo ahead of using masto.host to ensure there were recovery plans in place. 

*Your data is safe*

# Safety Nets

Recently we were updated to Mastodon 2.3.1 which includes an 'Export' feature under your profile settings. This feature will let you download *all* of your toots and information on Mastodon.

If you're ever concerned or want some peace of mind, please use this feature as a form of backup as well. The developers of Mastodon worked hard on the feature and from what we've seen, it's well put together.

# The Other Side

Hopefully this post helps shed a bit of light on how this instance is hosted behind the scenes. The circumstances surrounding the recent instance total failure (NOT US) are unfortunate and we hope we can all 'fail forward' with compassion, understanding and lessons learned.
