---
layout: post
title: Bots, OSS, Donations, Blocks
date: 2017-06-28
tags: [announce, announcement]
---

Today was a very productive day for getting stuff done 😀

## Bots
The admin team approved the first bot: [@Announce](https://photog.social/@announce) today. We may have written it ourselves 😉. It's a bot that mirrors the blog's RSS feed to the instance as toots. It'll also post some reminders and *ALL* major announcements. Major announcements will continue to come through [@Ambassador](https://photog.social/@Ambassador) as well.

We're hoping [@Announce](https://photog.social/@announce) will have better visibility for users when it comes to announcements and blog posts. [@Ambassador](https://photog.social/@Ambassador) is hard at work fav/boosting photography work and we feel announcements may get lost in the noise.

## Open Source Stuff
The team has decided to publish the website code (Jekyll) and bot code (Python) to GitHub as Open Source Software (OSS). Feel free to check it out [here (link)](https://github.com/photog-social). We've selected GPLv3 ([further reading](https://www.gnu.org/licenses/quick-guide-gplv3.en.html)) and CC BY-NC-SA 4.0 ([further reading](https://creativecommons.org/licenses/by-nc-sa/4.0/)) as the licenses for code and documentation respectively.

We plan to publish pretty much everything we do under open source and Creative Commons licenses. We've also added a GitHub badge to the footer of the site in case you're ever looking for a link in the future.

## Donations
We added a donations page with Patreon, Liberapay and PayPal options ([link](/donations)). The page can be reached from the header of the site if you're ever interested.

We ask that you only donate if you're feeling up for it. *NO* pressure at all. We are *NOT* asking for donations but we know some users will want to add to the tip jar.

## Instance Blocks
We were alerted to another Free Speech Zone™️ today and have instituted a silence for the instance. The instance is [anitwitter.moe](https://anitwitter.moe) for those curious.

We have a policy of silencing Free Speech Zones™️ to help prevent potentially toxic behavior from flooding the Federated Timeline.

# That's all folks
Happy Tooting!
