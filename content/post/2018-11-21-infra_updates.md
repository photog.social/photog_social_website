---
title: Infrastructure Updates
subtitle: More Soon
date: 2018-11-21
notoc: true
tags: [infrastructure,tech,announce,announcement,upgrades]
---

Today the Admin team is excited to announce we have migrated *all* secondary services to a new server.

Our galleries, alternate UIs and this blog are now hosted on a Scaleway ARM VPS in Paris.

This is the first major step towards improving some of our offerings. We have plans to deploy some moderation tools (more on that soon) as well as a Pixelfed instance (when federation is complete).

All told, our various services averaged less than 10 minutes of down time. We thank you for your patience and using our services.

