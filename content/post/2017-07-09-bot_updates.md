---
layout: post
title: New Bot
date: 2017-07-09
notoc: true
tags: [announce, announcement]
---

Today we're pleased to announce a Welcome Bot has been approved on the instance. If you're a first time poster the bot will send out a nice welcome message and post it to the public timeline.

We are hoping to see the instance grow and would like all new community members to be warmly welcomed. We also would like to see others 'notice' that a new user has popped up. We feel the best way to get this done was to create a welcome bot.

It also was an attempt to ensure the admin staff NEVER miss a warm welcome for new users 😜

Happy tooting!
