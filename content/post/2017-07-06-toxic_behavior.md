---
layout: post
title: Updates (Toxic Behavior)
date: 2017-07-06
notoc: true
tags: [announce, announcement]
---

# GitHub Toxic Behavior
Thanks to a [blog post (link)](http://where.coraline.codes/blog/my-year-at-github/) on 2017-07-05 about GitHubs behavior and internal workings the admin team has decided to move our source code from GitHub to GitLab. GitLab has shown they are a reasonable corporation and behave in a manner more suitable to the ethos of the Mastodon network.

As an admin team we strive to be inclusive and look past race, creed, color, gender, sexual preference, etc and focus on who a person truely is (personality, are you morally grounded, etc). GitHub has demonstrated a behavior that doesn't fit with our core beliefs and ethos. As such we cannot in good conscious continue to use their service.

# Source Code Move
Effective immediately our code has moved from GitHub to GitLab. The new code location can be found [here (link)](https://gitlab.com/photog.social). If GitLab demonstrates similar behavior, we'll find a new home. We are comitted to being a good member of the Mastodon community and only caring if an individual is a Good Person™️

# Going Forward
Going forward we ask that if you're made away of any toxic behavior from one of our service providers, you bring it to our attention. We can't stay on top of all news and being made aware of toxic behavior is something we encourage. Anything brought to our attention will be discussed, weighed and acted upon. There are a lot of providers out there and we intend to try to use only reasonable providers.

Our current list of providers:

- Namecheap
- CloudFlare
- Microsoft Azure
- Microsoft Office 365
- masto.host
- Mastodon

# Site Updates
While we were working on moving the sources to a new hosting provider we took the opportunity to update the site to include links to the new GitLab code as well as a link to the [@Ambassador@photog.social](https://photog.social/@ambassador) Mastodon admin account.
