---
layout: post
title: Silence/Suspension Updates
date: 2017-08-06
notoc: true
tags: [announce, announcement]
---

Quick update today.

We have *UN* silenced mastodon.starrevolution.org and ika.moe. We left the remote media block in place for ika.moe.

We had incorrectly identified these as 'bad' instances. Our apologies to the administrators of the instances.

We will do better about any blocks in the future.
