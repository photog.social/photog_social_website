---
title: Upgrades
subtitle: Yay!
date: 2018-03-28
tags: [infrastructure,tech,announce,announcement,upgrades]
---

# Instance Upgrade

The photog.social instance is hosted by [masto.host](https://masto.host) and today we upgraded to the next hosting level! Thank you to everyone who recently signed up and to those who've been around awhile. 😍

We were getting close to the 'limit' of our instance size and the admin team opted to upgrade. With all the wonderful people joining the federverse we wanted to be sure we did not run into any troubles.

If you know any photographers who are looking for a home on Mastodon, please recommend us 😀

# A Brif Introduction To Mastodon

We added a link to [@noelle@elekk.xyz](https://elekk.xyz/@noelle)'s wonderful [write up on Mastodon (link)](https://gist.github.com/joyeusenoelle/74f6e6c0f349651349a0df9ae4582969). We also linked to it in the ```Resources``` section of the header.

If you're new to Mastodon or wondering about some of our culture and memes, it's a great starting point.

# Fork Awesome

While we were working on the blog today we also migrated to [Fork Awesome](https://github.com/ForkAwesome/Fork-Awesome) from Font Awesome. The Fork Awesome devs have done a much better job keeping the icons up to date and adding new ones.

You shouldn't notice any real changes (except maybe the Mastodon logo in the footer 😉).
