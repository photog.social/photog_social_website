---
layout: post
title: Blocked Instances Updates
date: 2017-06-27
notoc: true
tags: [announce, announcement]
---

Nothing too special today, just some house keeping.

The list of blocked instances updated check out [the list (link)](/blocked_instances) for a full (now alphabetical! list) list.

Happy Tooting!
