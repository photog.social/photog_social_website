---
title: Smol site updates
subtitle: Now with fewer warnings
date: 2018-04-01
notoc: true
tags: [infrastructure,tech,announce,announcement,upgrades,donations]
---

Just a smol update today.

We got reports that Ad Blockers and Browsers were showing "**DIRE WARNING** OMG CRYPTO!!!111!!!" warnings to visitors of our blog.

We *did* have a browser based miner on our pages. It did **NOT** auto run. We used an approval only browser miner as a way to accept tips while people read our posts and/or browsed the blog. **IF** they wanted.

Again: *WE NEVER AUTO RAN/RUN MINING, IT'S OPT IN ONLY!*

Because the Ad Blockers, browsers and whatnot fail to pay attention and were scaring our users, supporters and others off... we disabled the applet on all but one of the pages here. It now only shows on a page linked from the Donations page.

You can still run browser mining as a donation method but it is only present on a dedicated page. Head to the [Donations (linky)](/donations) page for the link.

We appreciate the feedback and 💖 the support!
