---
layout: post
title: Silence/Suspension Updates
date: 2017-08-05
notoc: true
tags: [announce, announcement]
---

Quick update today.

We have *UN* silenced mstdn.jp and pawoo.net. We left the remote media block in place but the instances will no longer be silent. As of Mastodon 1.5.0 we can now only block the remote media while allowing accounts to interact.

Hopefully this helps with federation to our friends in Japan.
