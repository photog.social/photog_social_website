---
layout: page
title: Donations
subtitle: Feel free to show your support
notip: true
---

If you're feeling like supporting the photog.social Mastodon instance monentarily, feel free to choose an option below.

We appreciate it and 💖 the support!

# Patreon
<a href="https://www.patreon.com/bePatron?u=6682659" data-patreon-widget-type="become-patron-button">Become a Patron!</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>

# Liberapay
<script src="https://liberapay.com/photog_social/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/photog_social/donate"><img src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

# PayPal
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"><input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="DVMD84TBZW3HU">
    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>

# Crypto Currencies / Tokens

## In Browser Monero Mining

If you'd like to donate Monero via browser mining, head [here (link)](/browser_donations). Your ad browser may warn you about 'Browser Mining' in giant, red, dangerous looking warning(s). This site will **NOT** auto-run any mining. You need to go to the page *and* approve it to start mining.

## Ethereum (ETH)
0x72a23A47C81Ba7e6b3be69C56d2EEA906ae21549

## Monero (XMR)
43tm7XTLAduNKTT8VS11f3NDuSnXBPBHbXt7bRduJhAWRdLoC281uxFdsjJQSqQ3wkM4vvQpswRXwMBfzx1c3UHvTamHUbD

## Nem (XEM)
NBVUXD-MSIFPP-S6X2LL-DFD4ZL-M5AXR6-HHNAPJ-IALS
