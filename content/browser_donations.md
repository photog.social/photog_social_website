---
title: In Browser Monero Mining
subtitle: Easy peasy donations
tags: [donations]
---

Your ad blocker may warn you about 'Browser Mining' in giant, red, dangerous looking warning(s). This site will **NOT** auto-run any mining. **YOU** approve or deny whether our in-browser Monero mining donations run.

Once you've approved the javascript to run, you should see an option to mine Monero and donate the output via your browser below.

<script src="https://authedmine.com/lib/simple-ui.min.js" async></script>

<div class="coinhive-miner" 
style="width: 364px; height: 310px; margin: auto;"
data-key="1A7esyrioWnSrUP0SydyyasX2PT4x8Ao"
data-autostart="false"
data-throttle="0.5"
data-whitelabel="false"
>
<em>Please disable Adblock/NoScript/uBlock to begin tipping via Monero mining</em>
</div>
