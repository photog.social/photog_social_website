---
layout: page
title: Code of Conduct (CoC)
subtitle: Rules / regs / terms
---

<h2>Who We Are</h2>
A place for your photos and banter. Photog first is our motto

<ul>
    <li>photog.social doesn't monetize or profit off of your personal information</li>
    <li>Export and leave anytime</li>
    <li>Zero tolerance for harrassment or bullying, if you do this your account will be deleted</li>
    <li>All content is ⓒ each user and cannot be distributed or used without prior permission by the respective photog.social user</li>
    <li>By using this site you understand that it is not a backup or permanent repository for your information, statuses or media -- additionally, your media was downsized on original upload and is not exportable at this time upon transferring to new instance</li>
    <li>You may support the community by boosting and positively interacting with everyone</li>
    <li><em>No Porn</em></li>
    <li><em>No Loli</em></li>
    <li><em>No Bots w/o Approval</em></li>
    <li><em>Respect Others</em></li>
</ul>


<h2>General Information</h2>
<p>photog.social is a separate and independent instance and is not affiliated with the Mastodon organization.</p>

<p>Announcements can be found on the <a href="https://blog.photog.social/">blog (https://blog.photog.social/)</a></p>

<p>Blocked instances can be found <a href="http://blog.photog.social/blocked_instances/">here (link)</a></p>

<h2>Admin/Mod Team</h2>
<ul>
    <li><a href="https://photog.social/@Ambassador">@Ambassador</a>: Primary admin/final word</li>
</ul>

<h2>Approved Bots</h2>
<ul>
    <li><a href="https://photog.social/@announce">@Announce</a>: Announcements and other important info. Cross toots blog posts mainly.</li>
    <li><a href="https://photog.social/@dps_challenge">@dPS_Challenge</a>: Bot that cross posts Digital Photography School (dPS) weekly challenges.</li>
</ul>

<h2>Federation Policy and Terms of Use</h2>
<ol>
    <li>Federation serves as a browsing and temporary cached link to cited work here on photog.social</li>
    <li>Other instances may not disseminate others' work or toots without credit and proper licensing.</li>
    <li>Other instances may not use dumb code, machine learning or AI to analyze any derived text or work for publication or profit without photog.social user consent. This includes serving ads next to copyright content originally submitted on photog.social</li>
    <li>Public toots are derived works that are still copyright protected.</li>
    <li>Your instance will be <a href="http://blog.photog.social/blocked_instances/">blocked/suspended or silenced</a> for willingly allowing any of the content prohibited by this instance</li>
</ol>

<h2>Code of Conduct</h2>
<h2>Guidelines</h2>

<p>The following guidelines are not a legal document, and final interpretation is up to the administration of photog.social; they are here to provide you with an insight into the site and moderation policies</p>

<ol>
    <li>
        All users, local and remote, are expected to adhere to the following interaction principles:
        <ol type="a">
            <li>Posting work of others must be cited (or say artist/author unknown) after searching. Fair Use of copyright applies here.</li>
            <li>Respect boundaries as placed by others</li>
            <li>Be respectful/apologize if someone tells you that you cross a boundary accidentally</li>
            <li>Give people space if you accidentally push past a boundary</li>
            <li>If you are on either side of this and are worried about the situation escalating, contact the admin or a mod</li>
            <li>Tag potentially upsetting content such as violence/blood, politics, and other such topics</li>
        </ol>
    </li>

    <li>
        All users, local and remote, will be considered in the wrong in a conversation that goes sour if they participate in the following:
        <ol type="a">
            <li>Microaggressions ("not all men", white/man/cis-splaining, using the wrong pronouns to address someone after being corrected, etc)</li>
            <li>Demanding education on problematic behavior from a non-admin/moderator</li>
            <li>Skirting the lines of being a troll</li>
            <li>Not tagging potentially sensitive replies</li>
        </ol>
        This is not an exhaustive list.  If any of these don't make sense, please speak with an admin or moderator -- we are more than happy to help you understand the problems with these kinds of behavior.
    </li>

    <li>
        The following types of content will be removed from the public timeline:
        <ol type="a">
            <li>Advertising</li>
            <li>News bots that do not tag upsetting content</li>
            <li>Untagged pornography and sexually explicit content</li>
            <li>Untagged gore and extremely graphic violence</li>
        </ol>
    </li>

    <li>
        The following types of content will be removed from the public timeline, and may result in account suspension and revocation of access to the service:
        <ol type="a">
            <li>Racism or advocation of racism</li>
            <li>Sexism or advocation of sexism</li>
            <li>Discrimination against gender and sexual minorities, or advocation thereof</li>
            <li>Xenophobic and/or violent nationalism</li>
        </ol>
    </li>

    <li>
        The following types of content are explicitly disallowed and will result in revocation of access to the service:
        <ol type="a">
            <li>Sexual depictions of children</li>
            <li>Content illegal in Germany and/or France, such as holocaust denial or Nazi symbolism</li>
            <li>Conduct promoting the ideology of National Socialism</li>
        </ol>
    </li>

    <li>
        Any conduct intended to stalk or harass other users, or to impede other users from utilizing the service, or to degrade the performance of the service, or to harass other users, or to incite other users to perform any of the aforementioned actions, is also disallowed, and subject to punishment up to and including revocation of access to the service. This includes, but is not limited to, the following behaviors:
        <ol type="a">
            <li>Continuing to engage in conversation with a user that has specifically has requested for said engagement with that user to cease and desist may be considered harassment, regardless of platform-specific privacy tools employed.</li>
            <li>Aggregating, posting, and/or disseminating a person's demographic, personal, or private data without express permission (informally called doxing or dropping dox) may be considered harassment.</li>
            <li>Inciting users to engage another user in continued interaction or discussion after a user has requested for said engagement with that user to cease and desist (informally called brigading or dogpiling) may be considered harassment.</li>
        </ol>
    </li>
</ol>

<p>These provisions notwithstanding, the administration of the service reserves the right to revoke any user's access permissions, at any time, for any reason, except as limited by law.</p>
