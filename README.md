# What is this?

The [https://kemonine.info](https://kemonine.info) blog sources.

# License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />All markdown and imagery is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a> unless otherwise stated.

# Syndication

Content that has been approved for syndication is in the ```content/_syndication``` folder. Any content available for syndication is available under a more permissive license.

If you'd like to syndicate content from this repo under a more commercial friendly license, please reach out.
